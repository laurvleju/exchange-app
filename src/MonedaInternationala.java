public class MonedaInternationala {
    private String numeMoneda;
    private double monedaInternationalaToRON;

    public MonedaInternationala(String numeMoneda, double monedaInternationalaToRON) {
        this.numeMoneda = numeMoneda;
        this.monedaInternationalaToRON = monedaInternationalaToRON;
    }

    public String getNumeMoneda() {
        return numeMoneda;
    }

    public void setNumeMoneda(String numeMoneda) {
        this.numeMoneda = numeMoneda;
    }

    public double getMonedaInternationalaToRON() {
        return monedaInternationalaToRON;
    }

    public void setMonedaInternationalaToRON(double monedaInternationalaToRON) {
        this.monedaInternationalaToRON = monedaInternationalaToRON;
    }
}
