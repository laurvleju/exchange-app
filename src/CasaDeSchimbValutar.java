import java.util.Scanner;

public class CasaDeSchimbValutar {
    private int numarMonedeSuportate;
    private MonedaInternationala[] monedeInternationaleSuportate; // null
    private double[] rateSchimbRonToMonedeInternationale;// null

    private Scanner scannerText = new Scanner(System.in);
    private Scanner scannerNumere = new Scanner(System.in);

    public CasaDeSchimbValutar(int numarMonedeSuportate) {
        this.numarMonedeSuportate = numarMonedeSuportate;
        monedeInternationaleSuportate = new MonedaInternationala[numarMonedeSuportate];
        rateSchimbRonToMonedeInternationale = new double[numarMonedeSuportate];
    }

    //populam array-ul cu monedeInternationale
    public void populareMonedeSiRateDeSchimb(){
        System.out.println("Introdu monedele internationale suportate si ratele de schimb din ron catre acestea: ");
        int numarAfisare = 1;
        for (int i = 0; i < monedeInternationaleSuportate.length; i++) {
            System.out.println("Introdu moneda internationala nr " + numarAfisare + ": ");
            System.out.print("Nume moneda: ");
            String numeMoneda = scannerText.nextLine();
            System.out.print("Rata schimb catre ron: ");
            double rataRon = scannerNumere.nextDouble();
            MonedaInternationala monedaInternationalaCurenta = new MonedaInternationala(numeMoneda, rataRon);
            monedeInternationaleSuportate[i] = monedaInternationalaCurenta;
            numarAfisare++;

            System.out.print("Introdu rata de schimb din ron in " + monedaInternationalaCurenta.getNumeMoneda() + ": ");
            double ronToMonedaInternationalaCurenta = scannerNumere.nextDouble();
            rateSchimbRonToMonedeInternationale[i] = ronToMonedaInternationalaCurenta;
        }
    }

    public boolean monedaGasita(String moneda) {
        boolean monedaGasita = false;
        for (int i = 0; i < monedeInternationaleSuportate.length; i++) {
            if (moneda.equalsIgnoreCase(monedeInternationaleSuportate[i].getNumeMoneda())) {
                monedaGasita = true;
            }
        }
        return monedaGasita;
    }

    public void startExchangeV1() {
        System.out.print("Introdu moneda din care vrei sa schimbi: ");
        String dinMoneda = scannerText.nextLine();
        if (!monedaGasita(dinMoneda)) {
            System.out.println("Ne pare rau dar nu suportam moneda: " + dinMoneda + "! ");
            return;
        }
        System.out.print("Introdu moneda in care vrei sa schimbi: ");
        String inMoneda = scannerText.next();
        if (!monedaGasita(inMoneda)) {
            System.out.println("Ne pare rau dar nu suportam moneda: " + inMoneda + "! ");
            return;
        }
        // Schimbam din moneda internationala in RON !
        MonedaInternationala monedaInternationalaCurenta = null;
        for (int i = 0; i < monedeInternationaleSuportate.length; i++) {
            if (monedeInternationaleSuportate[i].getNumeMoneda().equalsIgnoreCase(dinMoneda)) {
                monedaInternationalaCurenta = monedeInternationaleSuportate[i];

            }
        }
        System.out.print("Introdu suma pe care vrei sa o schimbi(numar intreg): ");
        int sumaMonedaInternationala = (int) scannerNumere.nextDouble();
        double sumaRon = sumaMonedaInternationala * monedaInternationalaCurenta.getMonedaInternationalaToRON();
        System.out.println("Suma in ron intermediara este: " + sumaRon + " RON");
        int indexMonedaInCareConvertim = -1;
        for (int i = 0; i < monedeInternationaleSuportate.length; i++) {
            if(monedeInternationaleSuportate[i].getNumeMoneda().equalsIgnoreCase(inMoneda)){
                indexMonedaInCareConvertim = i;
            }
        }

        if (indexMonedaInCareConvertim != -1) {
            double sumaInMoneda = sumaRon * rateSchimbRonToMonedeInternationale[indexMonedaInCareConvertim];
            System.out.println("Suma dumnevoastra dupa schimbare este: " + sumaInMoneda +" " + inMoneda );
        }else {
            System.out.println("Nu s-a gasit rata de schimb din " + dinMoneda + " " + inMoneda);
        }


    }

    public int getNumarMonedeSuportate() {
        return numarMonedeSuportate;
    }

    public void setNumarMonedeSuportate(int numarMonedeSuportate) {
        this.numarMonedeSuportate = numarMonedeSuportate;
    }

    public MonedaInternationala[] getMonedeInternationaleSuportate() {
        return monedeInternationaleSuportate;
    }

    public void setMonedeInternationaleSuportate(MonedaInternationala[] monedeInternationaleSuportate) {
        this.monedeInternationaleSuportate = monedeInternationaleSuportate;
    }


    public void startExchangeV2() {
//        // NUME toRON .....
//        //{EURO 3.4, USD 2.4, GBP 4.3, AUD 3.4, YEN 2.3}
//        // 1.EURO -> USD
//        // 2.EURO -> GBP
//        // 3.EURO -> AUD
//
//        // 4 elemnte => 4 * 3
//        // 5 elemente => 5 * 4
//        // 6 elemente => 6 * 5
//
        int indexCurentAfisare = 1;
        int dimensuneArrayMeniu = numarMonedeSuportate * (numarMonedeSuportate - 1);
        String[] meniuMonedeDinCareConvertim = new String[dimensuneArrayMeniu];
        String[] meniuMonedeInCareConvertim = new String[dimensuneArrayMeniu];
        for (int i = 0; i < monedeInternationaleSuportate.length; i++) {
            for (int j = 0; j < monedeInternationaleSuportate.length; j++) {
                if (!monedeInternationaleSuportate[i].getNumeMoneda().equalsIgnoreCase(monedeInternationaleSuportate[j].getNumeMoneda())){
                    System.out.println(indexCurentAfisare + ". " + monedeInternationaleSuportate[i].getNumeMoneda() + " -> " + monedeInternationaleSuportate[j].getNumeMoneda());
                    meniuMonedeDinCareConvertim[indexCurentAfisare-1] = monedeInternationaleSuportate[i].getNumeMoneda();
                    meniuMonedeInCareConvertim[indexCurentAfisare-1] = monedeInternationaleSuportate[j].getNumeMoneda();
                    indexCurentAfisare++;
                }
            }
        }

        System.out.print("Alege o optiune din meniu: ");
        int optiuneAleasa = scannerNumere.nextInt();
        String dinMoneda = meniuMonedeDinCareConvertim[optiuneAleasa-1];
        String inMoneda = meniuMonedeInCareConvertim[optiuneAleasa-1];
        System.out.println("Din moneda: " + dinMoneda);
        System.out.println("In moneda: " + inMoneda);

        // Schimbam din moneda internationala in RON !
        MonedaInternationala monedaInternationalaCurenta = null;
        for (int i = 0; i < monedeInternationaleSuportate.length; i++) {
            if (monedeInternationaleSuportate[i].getNumeMoneda().equalsIgnoreCase(dinMoneda)) {
                monedaInternationalaCurenta = monedeInternationaleSuportate[i];

            }
        }
        System.out.print("Introdu suma pe care vrei sa o schimbi(numar intreg): ");
        int sumaMonedaInternationala = (int) scannerNumere.nextDouble();
        double sumaRon = sumaMonedaInternationala * monedaInternationalaCurenta.getMonedaInternationalaToRON();
        System.out.println("Suma in ron intermediara este: " + sumaRon + " RON");
        int indexMonedaInCareConvertim = -1;
        for (int i = 0; i < monedeInternationaleSuportate.length; i++) {
            if(monedeInternationaleSuportate[i].getNumeMoneda().equalsIgnoreCase(inMoneda)){
                indexMonedaInCareConvertim = i;
            }
        }

        if (indexMonedaInCareConvertim != -1) {
            double sumaInMoneda = sumaRon * rateSchimbRonToMonedeInternationale[indexMonedaInCareConvertim];
            System.out.println("Suma dumnevoastra dupa schimbare este: " + sumaInMoneda +" " + inMoneda );
        }else {
            System.out.println("Nu s-a gasit rata de schimb din " + dinMoneda + " " + inMoneda);
        }

    }
}
