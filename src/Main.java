import java.util.Scanner;

public class Main {
    //1. Money Exchange App
    //Casa de schimb va avea monede predefinite pe care le va accepta la schimb
    //Va avea monede in care se va putea schimba
    //Va avea rate de schimb pentru toate schimburile posibile
    //Ex:
    //monedainternationala -> clasa separata pentru monedainternationala -> nume si rata de schimb monedaInternationalaToRon
    //ron -> euro
    //ron -> usd
    //ron -> yen
    //ron -> gbp
    //Casa de schimb -> clasa separata -> array cu monede acceptate -> citite de la tastatura (array cu elemente MonedaInternationala)
    //MONEDA ACCEPATA EXEMPLU: {nume: "EURO" schimbcuRon: 3.4, "USD", "RON", "YEN", "GBP"} -> citit de la tastatura elementele array-ului , monedele suportate de casa de schimb
    //De fiecare daca cand se doreste schimbare dintr-o moneda internationala in alta moneda internationala, se va vace schimbul prin moneda nationala.
    //Dupa fiecare operatie se va intreba utilizatorul daca doreste sa mai efectueze o noua schimbare y/n
    public static void main(String[] args) {
        //sa creem un obiect de tip casa de schimb
        Scanner scannerNumere = new Scanner(System.in);
        System.out.print("Introdu numarul maxim de monede pe care le va suporta catre schimb casa valutara: ");
        int numarMonedeSuportate = scannerNumere.nextInt();
        CasaDeSchimbValutar casaDeSchimbValutar = new CasaDeSchimbValutar(numarMonedeSuportate);
        //sa populam array-ul cu monedeinternationale ale casei de schimb
        casaDeSchimbValutar.populareMonedeSiRateDeSchimb();
        casaDeSchimbValutar.startExchangeV1();
    }
}
